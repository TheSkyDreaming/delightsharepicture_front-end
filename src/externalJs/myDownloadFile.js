export function downloadFile (url, fileName) {
  const xhr = new XMLHttpRequest()
  xhr.open('GET', url, true)
  xhr.responseType = 'blob'
  xhr.onload = () => {
    if (xhr.status === 200) {
      // 获取文件blob数据并保存
      // var fileName = getFileName(url);
      saveAs(xhr.response, fileName)
    }
  }
  xhr.send()
}

/**
 * URL方式保存文件到本地
 * @param data 文件的blob数据
 * @param name 文件名
 */
function saveAs (data, name) {
  const urlObject = window.URL || window.webkitURL || window
  const exportBlob = new Blob([data])
  const saveLlink = document.createElementNS('http://www.w3.org/1999/xhtml', 'a')
  saveLlink.href = urlObject.createObjectURL(exportBlob)
  saveLlink.download = name
  saveLlink.click()
}
