import Vue from 'vue'
import VueRouter from 'vue-router'

import login from '../pages/login/login'
import adminLogin from '../pages/login/adminLogin'
import register from '../pages/register/register'
import index from '../pages/index/index'
import showResource from '../pages/index/showResource'
import showAll from '../pages/index/showAll'
import userCenter from '../pages/userCenter/userCenter'
import infoManage from '../pages/userCenter/infoManage'
import resManage from '../pages/userCenter/resManage'
import admin from '../pages/admin/admin'
import userManage from '../pages/admin/userManage'
import allResManage from '../pages/admin/allResManage'

Vue.use(VueRouter)

const routes = [
  // 经过试验发现redirect可以重定向到动态路由中
  { path: '/', redirect: '/index/showAll' },
  {
    path: '/index',
    component: index,
    children: [
      { path: 'showAll', component: showAll },
      { path: 'showResource/:showStyle/:showKey', component: showResource, props: true }
    ]
  },
  {
    path: '/login/:logStyle',
    component: login,
    props: true
  },
  {
    path: '/adminDelight',
    component: adminLogin
  },
  {
    path: '/register/:regStyle',
    component: register,
    props: true
  },
  {
    path: '/userCenter',
    component: userCenter,
    children: [
      { path: 'infoManage', component: infoManage },
      { path: 'resManage', component: resManage }
    ]
  },
  {
    path: '/admin',
    component: admin,
    children: [
      { path: 'userManage', component: userManage },
      { path: 'allResManage', component: allResManage }
    ]
  }
]

const router = new VueRouter({
  routes
})

// 任何人都可以访问的基url
const excludeUrl = [
  'index',
  'login',
  'register',
  'adminDelight'
]
// 路由导航守卫，将一些需要登录的路由重定向到登录页面,这里暂时没有考虑管理员的，以后再说
router.beforeEach(
  // 直接放行的url
  (to, from, next) => {
    const resIndex = excludeUrl.findIndex(
      elem => {
        if (to.path.includes(elem)) {
          return true
        }
      }
    )
    if (resIndex !== -1) {
      return next()
    }
    // 需要判断是否登录的url
    const myToken = window.sessionStorage.getItem('delightToken')
    if (myToken !== null) {
      return next()
    } else {
      return next(from.path)
    }
  }
)

export default router
