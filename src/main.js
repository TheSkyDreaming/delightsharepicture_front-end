import Vue from 'vue'
import App from './App.vue'
import router from './router/route'
import vuetify from './plugins/vuetify'
import './config/axiosConfig'

Vue.config.productionTip = false
new Vue({
  router,
  vuetify,
  render: h => h(App)
}).$mount('#app')
