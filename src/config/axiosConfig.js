import axios from 'axios'
import Vue from 'vue'

// 不需要加token的请求url。
const excludedRequestUrl = ['login', 'register']
// axios请求拦截器
axios.interceptors.request.use(
  conf => {
    const resIndex = excludedRequestUrl.findIndex(
      ele => {
        if (conf.url.includes(ele)) {
          return true
        }
      }
    )
    // 判断发送的请求是否为登录注册，如果是不是登录注册需要在请求头上加上token然后再放行，否则直接放行。
    if (resIndex === -1) {
      const token = window.sessionStorage.getItem('delightToken')
      conf.headers.Authorization_DelightXiao = token
    }
    return conf
  },
  err => {
    console.log('错误信息：' + err)
  }
)
//  axios的基准url,目前还没有做Nginx反向代理。
axios.defaults.baseURL = 'http://localhost:8320/'
// axios响应拦截器
axios.interceptors.response.use(
  axiosData => {
    return axiosData.data
  },
  err => {
    console.log('出啥子事了哦？这个：' + err)
  }
)
Vue.prototype.$http = axios
